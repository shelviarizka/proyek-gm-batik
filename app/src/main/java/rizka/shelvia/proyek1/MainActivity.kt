package rizka.shelvia.proyek1

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {


    lateinit var db :SQLiteDatabase
    lateinit var ft : FragmentTransaction
    lateinit var fraguser : fraguser
    lateinit var fragbar  : fragbar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fragbar = fragbar()
        fraguser = fraguser()

    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0?.itemId){
            R.id.itembarang->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fraguser).commit()
                frameLayout.setBackgroundColor(Color.argb(255,245,255,245))
                frameLayout.visibility= View.VISIBLE
            }
            R.id.itembarang->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragbar).commit()
                frameLayout.setBackgroundColor(Color.argb(255,245,255,245))
                frameLayout.visibility= View.VISIBLE


            }
            R.id.itemabout -> frameLayout.visibility = View.GONE

        }


        return true
    }
}
